import React, { Fragment } from 'react';
import { Form, Field } from 'react-final-form';

type FieldRenderProps = any;

const formValidation = (values: {
  user: { firstName: string; lastName: string };
  email: string;
}) => {
  const errors: object = {};
  const user = {
    firstName: '',
    lastName: '',
  };

  if (!values.user) {
    user.firstName = 'Required';
    user.lastName = 'Required';
  } else if (!values.user.firstName) {
    user.firstName = 'Required';
  } else if (!values.user.lastName) {
    user.lastName = 'Required';
  }

  if (Object.keys(user).length > 0) {
    Object.defineProperty(errors, 'user', { value: user, writable: false });
    // errors.user = user;
    return errors;
  } else {
    return new Promise(resolve => {
      setTimeout(() => {
        if (!values.email) {
          resolve({ email: 'required' });
        } else {
          resolve({});
        }
      }, 3000);
    });
  }
};

const Input: React.FunctionComponent<FieldRenderProps> = ({ input, meta, label }) => (
  <Fragment>
    <p>{label}</p>
    <input {...input} />
    {meta.error && meta.touched && <pre style={{ color: 'red' }}>{meta.error}</pre>}
  </Fragment>
);

interface ReactFinalFormProps {
  handleSubmit: any;
  pristine: any;
  reset: any;
  submitting: any;
}
interface ReactFinalFormState {
  initialValues: any;
}

interface formValuesType {
  user: object;
  readonly id: string; // неизменяемый
  color?: string; // необязательный
}

export class ReactFinalForm extends React.PureComponent<ReactFinalFormProps, ReactFinalFormState> {
  _formValues: formValuesType;

  constructor(props: any) {
    super(props);

    let formValues: object;

    try {
      const storageValues: any = localStorage.getItem('form');
      formValues = JSON.parse(storageValues) || {};
    } catch (e) {
      formValues = {};
    }

    this._formValues = {
      user: {},
      id: 'asd',
    };

    this.state = {
      initialValues: formValues,
    };
  }

  handleSubmit = (values: any) => {
    console.log('handleSubmit');
    console.log(values);
  };

  saveFormValues = (form: any): void => {
    // Object.defineProperty(, 'user', form.values.user);
    this._formValues.user = form.values.user;
    // console.log(form)
  };

  componentDidMount() {
    window.addEventListener('beforeunload', () => {
      localStorage.setItem('form', JSON.stringify(this._formValues));
    });
  }

  render() {
    const { initialValues } = this.state;

    console.log(initialValues);

    return (
      <Form
        debug={this.saveFormValues}
        initialValues={initialValues}
        onSubmit={this.handleSubmit}
        validate={formValidation}
        render={({ handleSubmit }) => (
          <form onSubmit={handleSubmit}>
            <div>
              <label htmlFor="">First name</label>
              <div>
                <Field
                  name={'user.fistName'}
                  component={Input}
                  type={'text'}
                  plaseholder={'First name'}
                  // validate={(value: string) => (value && value.length < 10 ? 'error' : null)}
                ></Field>
              </div>
            </div>
            <div>
              <label htmlFor="">Last Name</label>
              <div>
                <Field
                  name={'user.lastName'}
                  component={Input}
                  type={'text'}
                  plaseholder={'Last Name'}
                ></Field>
              </div>
            </div>
            <div>
              <label htmlFor="">Email</label>
              <div>
                <Field
                  name={'user.email'}
                  component={Input}
                  type={'text'}
                  plaseholder={'Email'}
                ></Field>
              </div>
            </div>
            <div>
              <div>
                <label>
                  <Field name={'sex'} component={'input'} type={'radio'} value={'male'}></Field>Male
                </label>
              </div>
              <div>
                <label>
                  <Field name={'sex'} component={'input'} type={'radio'} value={'female'}></Field>
                  Feemale
                </label>
              </div>
            </div>
            <div>
              <label>Favorite Color</label>
              <div>
                <Field name={'favoriteColor'} component={'select'}>
                  <option value=""></option>
                  <option value="ff0000">Red</option>
                  <option value="00ff00">Green</option>
                  <option value="0000ff">Blue</option>
                </Field>
              </div>
            </div>
            <div>
              <label htmlFor="employed">Employed</label>
              <div>
                <Field
                  name={'employed'}
                  id={'employed'}
                  component={'input'}
                  type={'checkbox'}
                ></Field>
              </div>
            </div>
            <div>
              <label htmlFor=""></label>
              <div>
                <Field name={'notes'} component={'textarea'}></Field>
              </div>
            </div>
            <div>
              <button type={'submit'}>Submit</button>
              <button type={'button'}>Clear values</button>
            </div>
          </form>
        )}
      ></Form>
    );
  }
}

export default ReactFinalForm;
