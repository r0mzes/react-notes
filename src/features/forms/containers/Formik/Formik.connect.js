import { compose, pure } from 'recompose';
import FormikContainer from './Formik';

const enchanced = compose(pure);

export const Formik = enchanced(FormikContainer);
export default Formik;
